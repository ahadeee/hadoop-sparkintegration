#!/usr/bin/env python
# coding: utf-8

# In[122]:


import pandas as pd


# In[123]:


import numpy as np


# In[124]:


from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, array
from pyspark.sql.types import StringType
from datetime import datetime as Date
from pyspark.sql.dataframe import DataFrame
spark = SparkSession.builder.appName('covid').getOrCreate()


# In[144]:


from hdfs import InsecureClient


# In[145]:


client_hdfs = InsecureClient('http://192.168.0.102:50070')


# In[148]:


with client_hdfs.read('/ASSIGNMENT/Covid_Analysis_DataSet.csv', encoding = 'utf-8') as reader:
    df = pd.read_csv(reader)
df.head()


# In[125]:


df = pd.read_csv("file:///home/hadoop/Desktop/ASSIGNMENT/Covid_Analysis_DataSet.csv")


# In[127]:


from pyspark.sql import SparkSession  
from pyspark.sql.types import StructType, StructField
from pyspark.sql.types import DoubleType, IntegerType, StringType ,FloatType ,TimestampType


# In[128]:


mySchema = StructType([ StructField("col1",  StringType(), True)                       ,StructField("col2",  IntegerType(), True)                       ,StructField("col3",  IntegerType(), True)                       ,StructField("col4",  IntegerType(), True)                       ,StructField("col5",  IntegerType(), True)                       ,StructField("col6",  IntegerType(), True)                       ,StructField("col7",  IntegerType(), True)                       ,StructField("col8",  StringType(), True)                       ,StructField("col9",  StringType(), True)                       ,StructField("col10", StringType(), True)                       ,StructField("col11", FloatType(), True)])


# In[129]:


scSpark = SparkSession     .builder     .appName("Python Spark SQL example: Reading CSV file with schema")     .config("spark.some.config.option", "some-value")     .getOrCreate()


# In[130]:


sdf = scSpark.createDataFrame(df , schema=mySchema)


# In[131]:


sdf.show(5)


# In[132]:


sdf.collect()






# In[133]:


import pyspark.sql.functions as F


# In[134]:


lates_date=sdf.agg(F.max('col1').alias('max_date'))
lates_date.show()


# In[135]:


lates_date.collect()


# In[136]:


lates_date[0]['max_data']


# In[137]:


type(lates_date)


# In[138]:


sdf.count()


# In[139]:


sdf_filtered = sdf.where(
      "col1 = '{}'".format(lates_date)
)
sdf_filtered.show(11)


# In[140]:


sdf_filtered.count()


# In[142]:


overall_stats=sdf_filtered.agg(
  F.sum("col5"),
  F.sum("col6")
)
overall_stats.show(1 ,False)


# In[143]:


sdf_filtered.orderBy("col8").show(10, False)


# In[ ]:




